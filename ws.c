/*
 * TDQC5
 * Bruce Cain
 *
 * Main driver for word sorter project.
 */

#include <stdio.h>
#include "sorter.h"

int
main(
    int argc,
    char *argv[])
{
    sortWords(argc, argv);
    return 0;
}
