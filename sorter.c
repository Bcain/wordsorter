/*
 * TDQC5
 * Bruce Cain
 *
 * Source code for sorter functions.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <time.h>
#include "sorter.h"

/* A file pointer type to handle compare functions. */
typedef int     (
    *fP)            (
    const void *left,
    const void *right);

static int
defaultCompare(
    const void *left,
    const void *right)
{
    /* 
     * The default Compare that is based on the ascii word value 
     *
     * This is the same for all the compare functions:
     *      const void *left is the left value qsort passes.
     *
     *
     *      const void *right is the right value qsort passes.
     */

    return strcmp(((Word *) left)->pWord, ((Word *) right)->pWord);
}

static int
lengthCompare(
    const void *left,
    const void *right)
{
    /* 
     * Length compare sorts by the words length of characters
     */

    return strlen(((Word *) left)->pWord) - strlen(((Word *) right)->pWord);
}

static int
swabbleCompare(
    const void *left,
    const void *right)
{
    /* 
     * swabble compare sorts the words based off Scrabble(R) letter values
     */

    static int      swabbleScores[26] = {
        1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3,
        1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10
    };

    char           *leftWord = ((Word *) left)->pWord, *rightWord =
        ((Word *) right)->pWord;
    int             leftValue = 0, rightValue = 0;

    for (size_t i = 0; i < strlen(leftWord); i++)
    {
        if (isalpha(leftWord[i]))
        {
            if (isupper(leftWord[i]))
            {
                leftValue += swabbleScores[(leftWord[i] - 'A')];
            }
            else
            {
                leftValue += swabbleScores[(leftWord[i] - 'a')];
            }
        }
    }

    for (size_t i = 0; i < strlen(rightWord); i++)
    {
        if (isalpha(rightWord[i]))
        {
            if (isupper(rightWord[i]))
            {
                rightValue += swabbleScores[(rightWord[i] - 'A')];
            }
            else
            {
                rightValue += swabbleScores[(rightWord[i] - 'a')];
            }
        }
    }

    return leftValue - rightValue;
}

/* Number compare compares leading digits and defualt to 0 if none are found */
static int
numberCompare(
    const void *left,
    const void *right)
{
    /* 
     * Number compare compares leading digits and defualt to 0 if none are found
     */

    int             leftNum = 0, rightNum = 0, digitCount = 0;
    char           *leftWord = ((Word *) left)->pWord, *rightWord =
        ((Word *) right)->pWord;
    char           *leftStrNum, *rightStrNum;

    for (size_t i = 0; i < strlen(leftWord); i++)
    {
        if (isdigit(leftWord[i]))
        {
            digitCount++;
        }
        else
        {
            break;
        }
    }

    if (digitCount != 0)
    {
        leftStrNum = calloc(sizeof(char), digitCount + 1);
        strncpy(leftStrNum, leftWord, digitCount);
        leftNum = atoi(leftStrNum);
        free(leftStrNum);
    }
    else
    {
        leftNum = -1;
    }

    digitCount = 0;

    for (size_t i = 0; i < strlen(rightWord); i++)
    {
        if (isdigit(rightWord[i]))
        {
            digitCount++;
        }
        else
        {
            break;
        }

    }

    if (digitCount != 0)
    {
        rightStrNum = calloc(sizeof(char), digitCount + 1);
        strncpy(rightStrNum, rightWord, digitCount);
        rightNum = atoi(rightStrNum);
        free(rightStrNum);
    }
    else
    {
        rightNum = -1;
    }

    return leftNum - rightNum;
}

static void
cleanUp(
    Word * words,
    size_t wordCount)
{
    /* 
     * Clean up will free up the struct Word array and its contents
     *
     * Word *words is a pointer to a array of Word structures
     *
     * size_t wordCount is a unsigned long value to total words in *words
     */

    for (size_t i = 0; i < wordCount; i++)
    {
        free(words[i].pWord);
    }

    free(words);
}

static void
printWords(
    size_t wordCount,
    Word * words,
    Flags * flags)
{
    /* 
     * print words will print in ascending order
     *
     * size_t wordCount is the total words in *words
     *
     * Word *words is a array of Word Structures
     *
     * Flags *flags is a pointer to a Flag structure that contains all
     * command line arguements
     */

    if (flags->cFlag)
    {
        if (flags->countUpTo > wordCount)
        {
            flags->countUpTo = wordCount;
        }

        for (size_t i = 0; i < flags->countUpTo; i++)
        {
            if (i > wordCount - 1)
            {
                break;
            }

            if (words[i].pWord[0] == '\0')
            {
                flags->countUpTo++;
                continue;
            }

            printf("%s\n", words[i].pWord);
        }
    }
    else
    {
        for (size_t i = 0; i < wordCount; i++)
        {
            if (words[i].pWord[0] == '\0')
            {
                continue;
            }

            printf("%s\n", words[i].pWord);
        }
    }
}

static void
printReverse(
    size_t wordCount,
    Word * words,
    Flags * flags)
{
    /* 
     * Print reverse will print the sorted list in descending order
     *
     * size_t wordCount is the total number of words in *words
     *
     * Word *words is a array of Word structures
     *
     * Flags *flags is a pointer to a Flags structure that holds the
     * command line flags.
     *
     */

    if (flags->cFlag)
    {
        if (flags->countUpTo > wordCount)
        {
            flags->countUpTo = wordCount;
        }
        for (int i = (int)wordCount - 1;
             i >= (int) (wordCount - flags->countUpTo); i--)
        {
            if (i < 0)
            {
                break;
            }

            if (words[i].pWord[0] == '\0')
            {
                flags->countUpTo++;
                continue;
            }

            printf("%s\n", words[i].pWord);
        }
    }
    else
    {
        for (int i = (int)wordCount - 1; i >= 0; i--)
        {
            if (words[i].pWord[0] == '\0')
            {
                continue;
            }

            printf("%s\n", words[i].pWord);
        }
    }
}

static int
checkInt(
    char *optarg)
{
    /*
     *  checkInt will check if a passed argument if its a integer
     *
     *  char *optarg is the argument to be checked
     */

    char            returnValue = 1;

    for (size_t i = 0; i < strlen(optarg); i++)
    {
        if (!isdigit(optarg[i]))
        {
            returnValue = 0;
            break;
        }
    }

    return returnValue;
}

/* Jay if you dont mind commenting on how to improve this function. 
 * Please and Thank you
 */

static void
nonAlphaStrip(
    char *word)
{
    /*
     *  nonAlphaStrip will strip nonalphanumeric values basd on c's
     *  isalnum
     *
     *  char *word is the word that is being stripped of nonalphanumeric
     *  characters
     */

    int             leftSide = 0, rightSide = 0;
    char           *tempWord;


    for (size_t i = 0; i < strlen(word); i++)
    {
        if (!(isalnum(word[i])))
        {
            leftSide++;
        }
        else
        {
            break;
        }
    }

    for (size_t i = strlen(word); i > 0; i--)
    {
        if (!(isalnum(word[i])))
        {
            rightSide++;
        }
        else
        {
            break;
        }
    }

    if (strlen(word) == 1)
    {
        return;
    }

    tempWord = malloc(strlen(word) - (leftSide + rightSide) + 3);
    memset(tempWord, '0', (strlen(word - (leftSide + rightSide) + 3)));

    if (!tempWord)
    {
        return;
    }

    for (size_t i = leftSide; i < (strlen(word) - rightSide + 1); i++)
    {
        tempWord[i - leftSide] = word[i];
    }

    strcpy(word, tempWord);
    free(tempWord);
}

static int
argParse(
    int const argc,
    char *argv[],
    fP * comparer,
    Flags * flags)
{
    /*
     *  argParse will parse the command line arguments
     *
     *  int const argc is a constant of the total number of command line arguments
     *
     *  char *argv[] is an array of character pointers that point to each command
     *  line argument
     *
     *  fP *comparer is a function pointer to type of the compare functions
     *
     *  Flags *flags is a struct pointer that contains all the command line argument
     *  flags
     */

    int             opt;

    opterr = 0;
    flags->cFlag = 0;
    flags->rFlag = 0;
    flags->uFlag = 0;
    *comparer = defaultCompare;

    while ((opt = getopt(argc, argv, ":rnlpsauhc:")) != -1)
    {
        switch (opt)
        {
        case 'c':
        {
            if (checkInt(optarg))
            {
                flags->countUpTo = atoi(optarg);
                flags->cFlag = 1;
            }
            else
            {
                printf("-c argument must be integer greater than 0.\n");
                /* Exit if -c argument is invalid */
                exit(3);
            }
            break;
        }
        case 'r':
        {
            flags->rFlag += 1;
            break;
        }
        case 'n':
        {
            *comparer = numberCompare;
            break;
        }
        case 'l':
        {
            *comparer = lengthCompare;
            break;
        }
        case 's':
        {
            *comparer = swabbleCompare;
            break;
        }
        case 'a':
        {
            *comparer = defaultCompare;
            break;
        }
        case 'u':
        {
            flags->uFlag = 1;
            break;
        }
        case 'p':
        {
            flags->pFlag = 1;
            break;
        }
        case 'h':
        {
            printf("-a     :: sorts by lexicographically\n");
            printf("-c <n> :: prints n words from sorted results\n");
            printf("-l     :: sorts by word length\n");
            printf("-n     :: sorts by leading digits\n");
            printf
                ("-p     :: remove leading and trailing non-alphanumeric values\n");
            printf("-r     :: rverses the order of the sorted list\n");
            printf("-s     :: sorts by Scrabble(R) total letter values\n");
            printf("-u     :: uniquely print off the results\n");
            printf("-h     :: prints help\n");
            exit(1);
            break;
        }
        case ':':
        {
            printf("-%c needs argument.\n", optopt);
            /* Exit if -c doesn't have argument. */
            exit(4);
        }
        default:
        {
            fprintf(stderr, "Unknown option %c -- use -h\n", optopt);
            exit(2);
        }
        }
    }


    return optind;
}

static void
readWords(
    Word ** words,
    size_t * wordCount,
    FILE * pFile,
    Flags * flags)
{
    /*
     *  readWords will read the words from the given input files
     *
     *  Word **words is a pointer->pointer->Word structure
     *
     *  size_t *wordCount is a pointer to size_t of the total words  int **words
     *
     *  FILE *pFile is the stream where the words are read from
     *
     *  Flags *flags is a pointer to a struct that contains all usable flags
     */

    char           *word, *nextWord, *line = NULL, sep[11] = "\n\t\v\f\r ";
    size_t          size = 0;
    static size_t   arraySize = 1, totalRealloc = 0;

    while (getline(&line, &size, pFile) != -1)
    {
        for (word = strtok_r(line, sep, &nextWord); word;
             word = strtok_r(NULL, sep, &nextWord))
        {
            (*words)[*wordCount].pWord = malloc(strlen(word) + 1);

            /* Check if malloc returned NULL */
            if ((*words)[*wordCount].pWord == NULL)
            {
                continue;
            }

            strcpy((*words)[*wordCount].pWord, word);
            if (flags->pFlag)
            {
                nonAlphaStrip((*words)[*wordCount].pWord);
            }

            (*wordCount)++;

            if (*wordCount >= arraySize)
            {
                totalRealloc++;
                if (arraySize > 10000)
                {
                    *words =
                        realloc(*words, sizeof(*words) * (*wordCount * 1.45));
                    arraySize = *wordCount * 1.45;
                }
                else
                {
                    *words =
                        realloc(*words, sizeof(*words) * (*wordCount * 2));
                    arraySize = *wordCount * 2;
                }
            }

        }
    }

    free(line);
}


void
sortWords(
    int argc,
    char *argv[])
{
    /*
     *  sortWords will produce a sorted list of words from the given files
     *
     *  int argc is the total number of command line arguments
     *
     *  char *argv[] is a array of pointers of character pointers the point
     *  to the command line arguments
     */

    FILE           *pFile;
    fP              comparer = NULL;
    Word           *words;
    Flags           flags;
    char            tempFile[30], *line = NULL;
    size_t          wordCount = 0, j, rawTime, size = 0;
    int             startFiles = 0;

    /* Allocate space for first word */
    words = malloc(sizeof(*words));

    /* Get the index of first file passed */
    startFiles = argParse(argc, argv, &comparer, &flags);

    for (int i = startFiles; i < argc; i++)
    {
        pFile = fopen(argv[i], "r");

        if (pFile)
        {
            readWords(&words, &wordCount, pFile, &flags);

            fclose(pFile);
        }
        else
        {
            printf("File %s could not be opened.\n", argv[i]);
        }
    }

    if (startFiles == argc)
    {
        rawTime = time(0);
        sprintf(tempFile, ".%lu", rawTime);
        tempFile[strlen(tempFile) - 1] = '\0';

        pFile = fopen(tempFile, "w");

        if (pFile)
        {

            while (getline(&line, &size, stdin) != -1)
            {
                fputs(line, pFile);
            }
            fclose(pFile);

            pFile = fopen(tempFile, "r");
            readWords(&words, &wordCount, pFile, &flags);
            fclose(pFile);
            remove(tempFile);
            free(line);

            /* Sperator from user input and sorted list */
            printf("=============================================\n");
        }
        else
        {
            printf("Couldn't create temp file to store stdin words\n.");
            /* Exit because for some reason couldn't create and open file */
            exit(15);
        }
    }


    if (flags.uFlag)
    {
        /* qsort words first to easily remove unique words */
        qsort(words, wordCount, sizeof(*words), defaultCompare);

        /* Dedupe word list */
        for (size_t i = 0; i < wordCount - 1; i++)
        {
            j = i + 1;

            if (j > wordCount)
            {
                break;
            }
            while (!strcmp(words[i].pWord, words[j].pWord))
            {
                words[j].pWord[0] = '\0';
                j++;
            }
        }
    }

    /* Sort the list how the user prompted to */
    qsort(words, wordCount, sizeof(*words), comparer);

    /* Determine how to print the word list */
    if (flags.rFlag % 2)
    {
        printReverse(wordCount, words, &flags);
    }
    else
    {
        printWords(wordCount, words, &flags);
    }

    /* Free up the array of word structs */
    cleanUp(words, wordCount);
}
