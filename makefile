CC = gcc
CFLAGS = $(CF) -Wall -Wextra -Wpedantic -Wwrite-strings -Wstack-usage=512 -Wfloat-equal -Waggregate-return -Winline
OUT = ws
SRC = ws.c sorter.c
DEPS = sorter.h
OBJ = ws.o sorter.o

all:
	$(CC) $(CFLAGS) $(SRC) -o $(OUT)

debug:
	$(CC) -g $(CFLAGS) $(SRC) -o $(OUT)

clean:
	-@rm -rf *.o
	-@rm ws
