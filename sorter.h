/*
 * Header file for sort functions.
 */

#ifndef SORTER_H
#define SORTER_H

/* Word will keep up with a single word read from a file or stdin */
typedef struct Word
{
    char           *pWord;
} Word;

/* Flags track the command line arguments set */
typedef struct Flags
{
    int             rFlag;
    char            uFlag;
    char            pFlag;
    char            cFlag;
    size_t          countUpTo;
} Flags;

/* 
 * sortWords takes command line arguments trys to read from a file else
 * it will accept standard input from the prompt.
 */
void            sortWords(
    int argc,
    char *argv[]);

#endif
